DROP TABLE IF EXISTS SUPERHERO;

CREATE TABLE SUPERHERO
(
    id            INT NOT NULL AUTO_INCREMENT,
    created_date  TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    name          VARCHAR(100) NOT NULL,
    name_superhero VARCHAR(100) NOT NULL,
    description   VARCHAR(500) NOT NULL,
    last_update   TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
    PRIMARY KEY (`id`)
);

-- INSERT INTO SUPERHERO(name, name_superhero, description) VALUES ('minData','Super Codigo','ejecuta super codigos');

