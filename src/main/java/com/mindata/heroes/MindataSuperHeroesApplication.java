package com.mindata.heroes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
@EnableCaching
@SpringBootApplication
public class MindataSuperHeroesApplication {

	public static void main(String[] args) {
		SpringApplication.run(MindataSuperHeroesApplication.class, args);
	}

}
