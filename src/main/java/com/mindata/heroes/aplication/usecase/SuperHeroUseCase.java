package com.mindata.heroes.aplication.usecase;

import com.mindata.heroes.aplication.port.in.SuperHeroQuery;
import com.mindata.heroes.aplication.port.out.*;
import com.mindata.heroes.domain.Superhero;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class SuperHeroUseCase implements SuperHeroQuery {

    private final SuperHeroCreateRepository superHeroCreateRepository;
    private final SuperHeroUpdateRepository superHeroUpdateRepository;
    private final SuperHeroSearchAllRepository superHeroSearchAllRepository;
    private final SuperHeroSearchForIdRepository superHeroSearchForIdRepository;
    private final SuperHerosearchForLikeRepository superHerosearchForLikeRepository;
    private final SuperHeroDeleteRepository superHeroDeleteRepository;


    public SuperHeroUseCase(SuperHeroCreateRepository superHeroCreateRepository,
                            SuperHeroUpdateRepository superHeroUpdateRepository,
                            SuperHeroSearchAllRepository superHeroSearchAllRepository,
                            SuperHeroSearchForIdRepository superHeroSearchForIdRepository,
                            SuperHerosearchForLikeRepository superHerosearchForLikeRepository,
                            SuperHeroDeleteRepository superHeroDeleteRepository) {
        this.superHeroCreateRepository = superHeroCreateRepository;
        this.superHeroUpdateRepository = superHeroUpdateRepository;
        this.superHeroSearchAllRepository = superHeroSearchAllRepository;
        this.superHeroSearchForIdRepository = superHeroSearchForIdRepository;
        this.superHerosearchForLikeRepository = superHerosearchForLikeRepository;
        this.superHeroDeleteRepository= superHeroDeleteRepository;
    }

    @Override
    public void createSuperHero(Superhero superhero) {
        superHeroCreateRepository.createExecute(superhero);
    }

    @Override
    public void updateSuperHero(Superhero superhero) {
        superHeroUpdateRepository.updateteExecute(superhero);
    }

    @Override
    public List<Superhero> searchAll() {
        return superHeroSearchAllRepository.searchAllSuperHero();
    }

    @Override
    public Superhero searchForId(long id) {
        return  superHeroSearchForIdRepository.searchForIdSuperHero(id);
    }

    @Override
    public List<Superhero> searchForLikeSuperHero(String namehero) {
        return superHerosearchForLikeRepository.searchForLikeSuperHero(namehero);
    }

    @Override
    public void deleteSuperHero(Superhero superhero) {
        superHeroDeleteRepository.deleteSuperHero(superhero);
    }
}
