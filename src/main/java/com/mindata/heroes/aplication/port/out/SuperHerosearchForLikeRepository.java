package com.mindata.heroes.aplication.port.out;

import com.mindata.heroes.domain.Superhero;

import java.util.List;

public interface SuperHerosearchForLikeRepository {
     List<Superhero> searchForLikeSuperHero(String namehero);

}
