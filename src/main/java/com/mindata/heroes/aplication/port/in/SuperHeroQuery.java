package com.mindata.heroes.aplication.port.in;

import com.mindata.heroes.domain.Superhero;

import java.util.List;

public interface SuperHeroQuery {
    void createSuperHero(Superhero superhero);

    void updateSuperHero(Superhero superhero);

    List<Superhero> searchAll();

    Superhero searchForId(long id);

    List<Superhero> searchForLikeSuperHero(String namesuperhero);

    void deleteSuperHero(Superhero superhero);
}
