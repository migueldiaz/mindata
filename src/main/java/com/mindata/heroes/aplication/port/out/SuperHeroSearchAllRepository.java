package com.mindata.heroes.aplication.port.out;

import com.mindata.heroes.domain.Superhero;

import java.util.List;

public interface SuperHeroSearchAllRepository {

    List<Superhero> searchAllSuperHero();
}
