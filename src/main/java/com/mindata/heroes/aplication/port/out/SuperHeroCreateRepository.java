package com.mindata.heroes.aplication.port.out;

import com.mindata.heroes.domain.Superhero;

public interface SuperHeroCreateRepository {
    void createExecute(Superhero superhero);
}
