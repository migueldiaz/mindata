package com.mindata.heroes.aplication.port.out;

import com.mindata.heroes.domain.Superhero;

public interface SuperHeroSearchForIdRepository {
    Superhero searchForIdSuperHero(long id);
}
