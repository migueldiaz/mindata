package com.mindata.heroes.domain;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Superhero implements Serializable {
    private static final long serialVersionUID = 6515992228349615877L;


    private long id;
    private Timestamp createdDate;
    private String name;
    private String nameSuperhero;
    private String description;
    private Timestamp lastUpdate;

    public Superhero(String name, String nameSuperhero, String description) {
        this.name=name;
        this.nameSuperhero=nameSuperhero;
        this.description=description;
    }
    public Superhero(long id, String name, String nameSuperhero, String description) {
        this.id=id;
        this.name=name;
        this.nameSuperhero=nameSuperhero;
        this.description=description;
    }



}
