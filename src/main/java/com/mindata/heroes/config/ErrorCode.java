package com.mindata.heroes.config;

import javax.validation.constraints.NotEmpty;

public enum ErrorCode {

    BAD_REQUEST(105, "La request esta mal formateada", "BAD_REQUEST"),
    INVALID_PARAMETERS_ERROR(110, "{}", "INVALID_PARAMETERS"),
    WEB_CLIENT_GENERIC(103, "Unexpected rest client error", "INTERNAL_SERVER_ERROR"),
    KAFKA_EXCEPTION(109, "Error interno de cache", "REDIS_EXCEPTION"),
    INTERNAL_ERROR(108,"Internal Error","INTERNAL_ERROR"),
    INVALID_FILTERS_ERROR(109, "Invalid filters", "INVALID_FILTERS"),
    DATA_ACCESS_ERROR(111, "Unable to access Account data", "DATA_ACCESS_ERROR"),
    DATA_DUPLICATE_USER(203 , "Usuario Duplicado", "DUPLICATE_USER"),
    DATA_EMPTY(104,"No Existe Datos para este Resultado", "NOT_FOUND");


    private final int value;
    private final String reasonPhrase;
    private final String code;

    ErrorCode(int value, String reasonPhrase, String code) {
        this.value = value;
        this.reasonPhrase = reasonPhrase;
        this.code = code;
    }

    public int value() {
        return this.value;
    }

    public String getReasonPhrase() {
        return this.reasonPhrase;
    }

    public String getCode() {
        return this.code;
    }
}
