package com.mindata.heroes.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.io.IOException;
import java.sql.SQLException;

@Slf4j
@ControllerAdvice
public class ErrorHandler {

    @ExceptionHandler(SQLException.class)
    public ResponseEntity<ErrorResponse> handle(SQLException ex) {
        log.error(HttpStatus.BAD_REQUEST.getReasonPhrase(), ex);
        return buildCustomResponseError(HttpStatus.BAD_REQUEST, ErrorCode.BAD_REQUEST,ex.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponse> handle(MethodArgumentNotValidException ex) {
        log.error(HttpStatus.BAD_REQUEST.getReasonPhrase(), ex);
        return buildResponseError(HttpStatus.BAD_REQUEST, ErrorCode.INVALID_PARAMETERS_ERROR);
    }
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<ErrorResponse> handle(MissingServletRequestParameterException ex) {
        log.error(ErrorCode.INVALID_PARAMETERS_ERROR.getCode(), ex);
        return buildResponseError(HttpStatus.BAD_REQUEST, ErrorCode.INVALID_PARAMETERS_ERROR);
    }

    @ExceptionHandler(InvalidDataAccessApiUsageException.class)
    public ResponseEntity<ErrorResponse> handle(InvalidDataAccessApiUsageException ex) {
        log.error(ErrorCode.INVALID_PARAMETERS_ERROR.getCode(), ex);
        return buildResponseError(HttpStatus.BAD_REQUEST, ErrorCode.INVALID_PARAMETERS_ERROR);
    }

    @ExceptionHandler(EmptyResultDataAccessException.class)
    public ResponseEntity<ErrorResponse> handle(EmptyResultDataAccessException ex) {
        log.error(ErrorCode.DATA_EMPTY.getCode(), ex);
        return buildResponseError(HttpStatus.NOT_FOUND, ErrorCode.DATA_EMPTY);
    }

    @ExceptionHandler(IOException.class)
    public ResponseEntity<ErrorResponse> handle(IOException ex) {
        log.error(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(), ex);
        return buildCustomResponseError(HttpStatus.PAYMENT_REQUIRED, ErrorCode.INVALID_PARAMETERS_ERROR, ex.getMessage());
    }

    private ResponseEntity<ErrorResponse> buildCustomResponseError(HttpStatus httpStatus, ErrorCode errorCode, String customDescription) {
        final var response = ErrorResponse.builder()
                .errorInternalCode(errorCode.value())
                .errorDescription(customDescription)
                .errorCode(errorCode.getCode())
                .build();

        return new ResponseEntity<>(response, httpStatus);
    }

    private ResponseEntity<ErrorResponse> buildResponseError(HttpStatus httpStatus, ErrorCode errorCode) {
        return buildCustomResponseError(httpStatus, errorCode, errorCode.getReasonPhrase());
    }


    @Builder
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    private static class ErrorResponse {
        @JsonProperty
        int errorInternalCode;
        @JsonProperty
        String errorDescription;
        @JsonProperty
        String errorCode;
    }
}

