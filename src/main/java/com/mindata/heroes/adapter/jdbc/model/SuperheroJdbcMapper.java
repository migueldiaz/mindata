package com.mindata.heroes.adapter.jdbc.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SuperheroJdbcMapper implements RowMapper<SuperheroJdbcModel> {
    @Override
    public SuperheroJdbcModel mapRow(ResultSet rs, int rowNum) throws SQLException {
        return SuperheroJdbcModel.builder()
                .id(rs.getInt("id"))
                .createdDate(rs.getTimestamp("created_date"))
                .name(rs.getNString("name"))
                .nameSuperhero(rs.getString("name_superhero"))
                .description(rs.getString("description"))
                .lastUpdate(rs.getTimestamp("last_update"))
                .build();
    }
}
