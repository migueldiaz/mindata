package com.mindata.heroes.adapter.jdbc;

import com.mindata.heroes.adapter.jdbc.model.ParameterSourceSql;
import com.mindata.heroes.adapter.jdbc.model.SuperheroJdbcMapper;
import com.mindata.heroes.adapter.jdbc.model.SuperheroJdbcModel;
import com.mindata.heroes.aplication.port.out.SuperHeroSearchForIdRepository;
import com.mindata.heroes.config.CacheConfig;
import com.mindata.heroes.config.SqlReader;
import com.mindata.heroes.domain.Superhero;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@Slf4j
public class SuperHeroForIdJdbcAdapter implements SuperHeroSearchForIdRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final ParameterSourceSql parameterSourceSql;

    private static final String SEARCH_FOR_ID_SUPER_HERO = "sql/searhForIdSuperHero.sql";
    private String searchForIdSuperHeroSubcribeQuery;

    public SuperHeroForIdJdbcAdapter(NamedParameterJdbcTemplate jdbcTemplate, ParameterSourceSql parameterSourceSql) {
        this.jdbcTemplate = jdbcTemplate;
        this.parameterSourceSql = parameterSourceSql;
        this.searchForIdSuperHeroSubcribeQuery = SqlReader.readSql(SEARCH_FOR_ID_SUPER_HERO);
    }

    @Override
    @Cacheable(cacheNames = CacheConfig.USER_CACHE, key = "#id", unless = "#result == null")
    public Superhero searchForIdSuperHero(long id) {

        Optional<SuperheroJdbcModel> superherOptionalJdbcModel = validadorOptional(id);

        if (superherOptionalJdbcModel.isPresent()) {
            Superhero superhero = superherOptionalJdbcModel.get().toDomain();
            return superhero;
        };

        return null;
    }

    Optional<SuperheroJdbcModel> validadorOptional(long id){
        Optional<SuperheroJdbcModel> optional = Optional.of(Optional.ofNullable(jdbcTemplate.queryForObject(
                searchForIdSuperHeroSubcribeQuery,
                parameterSourceSql.getSqlParameterSourceForId(id),
                new SuperheroJdbcMapper())).orElseThrow());
        return optional;
    }
}
