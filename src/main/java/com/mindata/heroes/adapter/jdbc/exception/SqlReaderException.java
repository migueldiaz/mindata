package com.mindata.heroes.adapter.jdbc.exception;

public class SqlReaderException extends RuntimeException {
    public SqlReaderException(Throwable ex) {
        super(ex);
    }
}
