package com.mindata.heroes.adapter.jdbc;

import com.mindata.heroes.adapter.jdbc.model.ParameterSourceSql;
import com.mindata.heroes.adapter.jdbc.model.SuperheroJdbcMapper;
import com.mindata.heroes.adapter.jdbc.model.SuperheroJdbcModel;
import com.mindata.heroes.aplication.port.out.SuperHeroSearchAllRepository;
import com.mindata.heroes.config.CacheConfig;
import com.mindata.heroes.config.SqlReader;
import com.mindata.heroes.domain.Superhero;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
@Slf4j
public class SearchAllSuperHeroJdbcAdapter implements SuperHeroSearchAllRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final ParameterSourceSql parameterSourceSql;

    private static final String SEARCH_ALL_SUPER_HERO = "sql/searhSuperHeroAll.sql";
    private String searchAllSuperHeroSubcribeQuery;

    public SearchAllSuperHeroJdbcAdapter(NamedParameterJdbcTemplate jdbcTemplate, ParameterSourceSql parameterSourceSql) {
        this.jdbcTemplate = jdbcTemplate;
        this.parameterSourceSql = parameterSourceSql;
        this.searchAllSuperHeroSubcribeQuery = SqlReader.readSql(SEARCH_ALL_SUPER_HERO);
    }


    @Override
    @Cacheable(cacheNames = CacheConfig.USER_CACHE, unless = "#result == null")
    public List<Superhero> searchAllSuperHero() {

        var list = jdbcTemplate.query(searchAllSuperHeroSubcribeQuery, new SuperheroJdbcMapper())
                .stream()
                .map(SuperheroJdbcModel::toDomain)
                .collect(Collectors.toList());

        return list;

    }
}
