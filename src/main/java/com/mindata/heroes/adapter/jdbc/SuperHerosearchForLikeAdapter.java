package com.mindata.heroes.adapter.jdbc;

import com.mindata.heroes.adapter.jdbc.model.ParameterSourceSql;
import com.mindata.heroes.adapter.jdbc.model.SuperheroJdbcMapper;
import com.mindata.heroes.adapter.jdbc.model.SuperheroJdbcModel;
import com.mindata.heroes.aplication.port.out.SuperHerosearchForLikeRepository;
import com.mindata.heroes.config.CacheConfig;
import com.mindata.heroes.config.SqlReader;
import com.mindata.heroes.domain.Superhero;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
@Slf4j
public class SuperHerosearchForLikeAdapter implements SuperHerosearchForLikeRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final ParameterSourceSql parameterSourceSql;

    private static final String SEARCH_LIKE_SUPER_HERO = "sql/searhLikeSuperHero.sql";
    private String searchLikeSuperHeroSubcribeQuery;

    public SuperHerosearchForLikeAdapter(NamedParameterJdbcTemplate jdbcTemplate, ParameterSourceSql parameterSourceSql) {
        this.jdbcTemplate = jdbcTemplate;
        this.parameterSourceSql = parameterSourceSql;
        this.searchLikeSuperHeroSubcribeQuery = SqlReader.readSql(SEARCH_LIKE_SUPER_HERO);
    }

    @Override
    @Cacheable(cacheNames = CacheConfig.USER_CACHE, key = "#nameHero", unless = "#result == null")
    public List<Superhero> searchForLikeSuperHero(String nameHero) {

        Optional<List<SuperheroJdbcModel>> superheroOptionalJdbcModel = validadorOptionalLikeSuperHero(nameHero);

        if (superheroOptionalJdbcModel.isPresent()) {
            List<Superhero> superheroList = superheroOptionalJdbcModel.get().stream()
                    .map(SuperheroJdbcModel::toDomain)
                    .collect(Collectors.toList());
            return superheroList;
        }else {
            throw new EmptyResultDataAccessException(0);
        }
    }

    Optional<List<SuperheroJdbcModel>> validadorOptionalLikeSuperHero(String nameHero) {

        Optional<List<SuperheroJdbcModel>> optional = Optional.of(
                Optional.ofNullable(
                        jdbcTemplate.query(
                                searchLikeSuperHeroSubcribeQuery,
                                parameterSourceSql.getSqlParameterSourceForLike(nameHero),
                                new SuperheroJdbcMapper())).orElseThrow());

        return optional;
    }
}