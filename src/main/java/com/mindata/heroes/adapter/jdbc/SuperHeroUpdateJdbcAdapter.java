package com.mindata.heroes.adapter.jdbc;

import com.mindata.heroes.adapter.jdbc.model.ParameterSourceSql;
import com.mindata.heroes.aplication.port.out.SuperHeroCreateRepository;
import com.mindata.heroes.aplication.port.out.SuperHeroUpdateRepository;
import com.mindata.heroes.config.CacheConfig;
import com.mindata.heroes.config.SqlReader;
import com.mindata.heroes.domain.Superhero;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CachePut;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
@Slf4j
public class SuperHeroUpdateJdbcAdapter implements SuperHeroUpdateRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final ParameterSourceSql parameterSourceSql;

    private static final String UPDATE_SUPER_HERO = "sql/updateSuperhero.sql";
    private String updateSuperHeroSubcribeQuery;

    public SuperHeroUpdateJdbcAdapter(NamedParameterJdbcTemplate jdbcTemplate, ParameterSourceSql parameterSourceSql){
        this.jdbcTemplate = jdbcTemplate;
        this.parameterSourceSql = parameterSourceSql;
        this.updateSuperHeroSubcribeQuery = SqlReader.readSql(UPDATE_SUPER_HERO);
    }

    @Override
    @CachePut(cacheNames = CacheConfig.USER_CACHE, key = "#superhero.id")
    public void updateteExecute(Superhero superhero) {
        jdbcTemplate.update(updateSuperHeroSubcribeQuery, parameterSourceSql.getSqlParameterSource(superhero));
        log.info("Usuario Modificado");
    }
}
