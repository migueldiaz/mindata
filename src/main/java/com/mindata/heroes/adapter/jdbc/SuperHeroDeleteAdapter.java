package com.mindata.heroes.adapter.jdbc;

import com.mindata.heroes.adapter.jdbc.model.ParameterSourceSql;
import com.mindata.heroes.aplication.port.out.SuperHeroDeleteRepository;
import com.mindata.heroes.config.CacheConfig;
import com.mindata.heroes.config.SqlReader;
import com.mindata.heroes.domain.Superhero;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
@Slf4j
public class SuperHeroDeleteAdapter implements SuperHeroDeleteRepository {
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final ParameterSourceSql parameterSourceSql;

    private static final String DELETE_SUPER_HERO = "sql/deleteSuperHero.sql";
    private String deleteSuperHeroSubcribeQuery;

    public SuperHeroDeleteAdapter(NamedParameterJdbcTemplate jdbcTemplate, ParameterSourceSql parameterSourceSql) {
        this.jdbcTemplate = jdbcTemplate;
        this.parameterSourceSql = parameterSourceSql;
        this.deleteSuperHeroSubcribeQuery = SqlReader.readSql(DELETE_SUPER_HERO);
    }

    @Override
    @CacheEvict(cacheNames=CacheConfig.USER_CACHE,  key = "#superhero.id")
    public void deleteSuperHero(Superhero superhero) {
        jdbcTemplate.update(deleteSuperHeroSubcribeQuery, parameterSourceSql.getSqlParameterSource(superhero));
        log.info("Usuario Eliminado");
    }
}
