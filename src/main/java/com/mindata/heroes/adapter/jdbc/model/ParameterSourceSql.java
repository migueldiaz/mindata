package com.mindata.heroes.adapter.jdbc.model;

import com.mindata.heroes.domain.Superhero;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ParameterSourceSql {

    public MapSqlParameterSource getSqlParameterSource(Superhero superhero) {
        var sqlParameterSource = new MapSqlParameterSource();

        if (superhero.getId() > 0) {
            sqlParameterSource.addValue("id", superhero.getId());
        }

        sqlParameterSource.addValue("name", superhero.getName());
        sqlParameterSource.addValue("name_superhero", superhero.getNameSuperhero());
        sqlParameterSource.addValue("description", superhero.getDescription());
        log.info("Scrips SQL {}", sqlParameterSource);

        return sqlParameterSource;

    }

    public MapSqlParameterSource getSqlParameterSourceForId(long id) {
        var sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("id", id);
        log.info("Scrips SQL For id {}", sqlParameterSource);

        return sqlParameterSource;

    }

    public MapSqlParameterSource getSqlParameterSourceForLike(String nameSuperHero) {
        var sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("name_superhero", nameSuperHero);
        log.info("Scrips SQL like {}", sqlParameterSource);

        return sqlParameterSource;

    }
}
