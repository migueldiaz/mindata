package com.mindata.heroes.adapter.jdbc.model;


import com.mindata.heroes.domain.Superhero;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.sql.Timestamp;
import java.util.Date;

@Data
@Builder
public class SuperheroJdbcModel {

    private long id;
    private Timestamp createdDate;
    @NotBlank(message = "Campo name es requerido")
    private String name;
    @NotBlank(message = "Campo name es requerido")
    private String nameSuperhero;
    @NotBlank(message = "Campo name es requerido")
    private String description;
    private Timestamp lastUpdate;

    public Superhero toDomain(){
        return Superhero.builder()
                .id(id)
                .createdDate(createdDate)
                .name(name)
                .nameSuperhero(nameSuperhero)
                .description(description)
                .lastUpdate(lastUpdate)
                .build();
    }

}
