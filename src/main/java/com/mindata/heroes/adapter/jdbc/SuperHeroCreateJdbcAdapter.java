package com.mindata.heroes.adapter.jdbc;

import com.mindata.heroes.adapter.jdbc.model.ParameterSourceSql;
import com.mindata.heroes.aplication.port.out.SuperHeroCreateRepository;
import com.mindata.heroes.config.SqlReader;
import com.mindata.heroes.domain.Superhero;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
@Slf4j
public class SuperHeroCreateJdbcAdapter  implements SuperHeroCreateRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final ParameterSourceSql parameterSourceSql;
    private static final String CREATE_SUPER_HERO = "sql/createSuperhero.sql";
    private String createSuperHeroSubcribeQuery;

    public SuperHeroCreateJdbcAdapter(NamedParameterJdbcTemplate jdbcTemplate, ParameterSourceSql parameterSourceSql){
        this.jdbcTemplate = jdbcTemplate;
        this.parameterSourceSql = parameterSourceSql;
        this.createSuperHeroSubcribeQuery = SqlReader.readSql(CREATE_SUPER_HERO);
    }

    @Override
    public void createExecute(Superhero superhero) {
        jdbcTemplate.update(createSuperHeroSubcribeQuery, parameterSourceSql.getSqlParameterSource(superhero));
        log.info("Usuario Guardado");
    }


}
