package com.mindata.heroes.adapter.controller;

import com.mindata.heroes.adapter.controller.Anotation.AnnotationModelAdapter;
import com.mindata.heroes.adapter.controller.Anotation.TimeSuper;
import com.mindata.heroes.aplication.port.in.SuperHeroQuery;
import com.mindata.heroes.domain.Superhero;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Slf4j
@RequestMapping("api/superhero")
public class SuperHeroControllerAdapter {

    private final SuperHeroQuery superHeroQuery;
    private AnnotationModelAdapter anotationModelAdapter = new AnnotationModelAdapter();

    public SuperHeroControllerAdapter(SuperHeroQuery superHeroQuery) {
        this.superHeroQuery = superHeroQuery;
    }

    @TimeSuper
    @PostMapping("/create")
    public ResponseEntity<Superhero> create(@Valid @RequestBody Superhero superhero) {
        var inicio = System.currentTimeMillis();
        var sMethodName = new String (Thread.currentThread().getStackTrace()[1].getMethodName());
        superHeroQuery.createSuperHero(superhero);
        anotationModelAdapter.logicAnnotationTimeSuper(inicio, sMethodName);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @TimeSuper
    @PutMapping("/update")
    public ResponseEntity<Superhero> update(@Valid @RequestBody Superhero superhero) {
        var inicio = System.currentTimeMillis();
        var sMethodName = new String (Thread.currentThread().getStackTrace()[1].getMethodName());
        superHeroQuery.updateSuperHero(superhero);
        anotationModelAdapter.logicAnnotationTimeSuper(inicio,sMethodName);
        return new ResponseEntity<>(superhero, HttpStatus.OK);
    }

    @TimeSuper
    @GetMapping("/search")
    public ResponseEntity<Superhero> searchAll() {
        var inicio = System.currentTimeMillis();
        var sMethodName = new String (Thread.currentThread().getStackTrace()[1].getMethodName());
        var superheroes = superHeroQuery.searchAll();
        anotationModelAdapter.logicAnnotationTimeSuper(inicio,sMethodName);
        return new ResponseEntity(superheroes, HttpStatus.OK);
    }

    @TimeSuper
    @GetMapping("/searchid/{id}")
    public ResponseEntity<Superhero> searchForId(@Valid @PathVariable("id") long id) {
        var inicio = System.currentTimeMillis();
        var sMethodName = new String (Thread.currentThread().getStackTrace()[1].getMethodName());
        var superhero = superHeroQuery.searchForId(id);
        anotationModelAdapter.logicAnnotationTimeSuper(inicio,sMethodName);
        return new ResponseEntity(superhero, HttpStatus.OK);
    }

    @TimeSuper
    @GetMapping("/searchlike/{namesuperhero}")
    public ResponseEntity<Superhero> searchForLikeSuperHero(@PathVariable("namesuperhero") String namesuperhero) {
        var inicio = System.currentTimeMillis();
        var sMethodName = new String (Thread.currentThread().getStackTrace()[1].getMethodName());
        var superheroes = superHeroQuery.searchForLikeSuperHero(namesuperhero);
        anotationModelAdapter.logicAnnotationTimeSuper(inicio,sMethodName);
        return new ResponseEntity(superheroes, HttpStatus.OK);
    }


    @DeleteMapping("/delete")
    public ResponseEntity<Superhero> deleteSuperHero(@Valid @RequestBody Superhero superhero) {
        var inicio = System.currentTimeMillis();
        var sMethodName = new String (Thread.currentThread().getStackTrace()[1].getMethodName());
        superHeroQuery.deleteSuperHero(superhero);
        anotationModelAdapter.logicAnnotationTimeSuper(inicio,sMethodName);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

}
