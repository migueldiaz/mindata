package com.mindata.heroes.adapter.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("api/superhero/caches")
public class CacheAdapterController {

	private CacheManager cacheManager;

	public CacheAdapterController(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
	@PatchMapping("/{name}")
	public void evictCache(@PathVariable String name) {
		this.cacheManager.getCache(name).clear();
		log.info("Cache Borrado con exito");
	}

}
