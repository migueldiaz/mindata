package com.mindata.heroes.adapter.controller.Anotation;

import com.mindata.heroes.adapter.controller.SuperHeroControllerAdapter;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Method;

@Slf4j
public class AnnotationModelAdapter {

    public void logicAnnotationTimeSuper(long inicio, String sMethodName) {
        for (Method m : SuperHeroControllerAdapter.class.getDeclaredMethods()) {
            if (sMethodName.equals(m.getName())) {
                if (m.isAnnotationPresent(TimeSuper.class)) {
                    long fin = System.currentTimeMillis();
                    log.info("the task has taken {} milliseconds, for method name {}  ",(fin - inicio), m.getName());
                }
            }
        }
    }
}
