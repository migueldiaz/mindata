package com.mindata.heroes.config;

import com.mindata.heroes.adapter.jdbc.exception.SqlReaderException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

class SqlReaderTest {

    @Test
    @DisplayName("Dado un path a un archivo sql, se devuelve un String con el contenido")
    void readFileOk() {
        String sqlFilePath = "sql/testSqlReader.sql";
        String expectedContent = "SELECT 1 FROM SuperHero;";
        String content = SqlReader.readSql(sqlFilePath);

        assertThat(content).isNotNull();
        assertThat(content).isEqualTo(expectedContent);
    }

    @Test
    @DisplayName("Dado un error de lectura, se dispara una SqlReaderException")
    void readFileError() {
        String sqlFilePath = "sql/inexistent.sql";
        Throwable thrown = catchThrowable(() -> SqlReader.readSql(sqlFilePath));
        assertThat(thrown)
            .isInstanceOf(SqlReaderException.class);
    }

}
