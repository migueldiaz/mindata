package com.mindata.heroes.adapter.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mindata.heroes.aplication.port.in.SuperHeroQuery;
import com.mindata.heroes.domain.Superhero;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(SuperHeroControllerAdapter.class)
public class SuperHeroControllerAdapterTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SuperHeroQuery superHeroQuery;

    @Test
    @DisplayName("api para Crear superHero")
    public void create() throws Exception {
        Superhero esperado = new Superhero("Clack Ken", "Superman", "El Hombre de Acero");
        mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/superhero/create")
                        .content(asJsonString(esperado))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
        Mockito.verify(superHeroQuery).createSuperHero(esperado);
    }

    @Test
    @DisplayName("api para Actualizar superHero")
    public void update() throws Exception {
        Superhero esperado = new Superhero(1l, "Clark Kent", "Superman", "El Hombre de Acero Regresa");
        mockMvc.perform(MockMvcRequestBuilders
                        .put("/api/superhero/update")
                        .content(asJsonString(esperado))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(esperado.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(esperado.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.nameSuperhero").value(esperado.getNameSuperhero()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.description").value(esperado.getDescription()));
        Mockito.verify(superHeroQuery).updateSuperHero(esperado);
    }

    @Test
    @DisplayName("api para Buscar Todos los SuperHeroes")
    public void search() throws Exception {
        Superhero esperado = new Superhero(1l, "Clark Kent", "Superman", "El Hombre de Acero Regresa");
        mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/superhero/create")
                        .content(asJsonString(esperado))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/superhero/search")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        Mockito.verify(superHeroQuery).searchAll();
    }

    @Test
    @DisplayName("api buscar superHero por Id")
    public void searchForId() throws Exception {
        Superhero esperado = new Superhero(1l, "Clark Kent", "Superman", "El Hombre de Acero Regresa");

        when(superHeroQuery.searchForId(1l)).thenReturn(esperado);

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/superhero/searchid/{id}", 1l)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(esperado.getId()))
                .andExpect(jsonPath("$.name").value(esperado.getName()))
                .andExpect(jsonPath("$.nameSuperhero").value(esperado.getNameSuperhero()))
                .andExpect(jsonPath("$.description").value(esperado.getDescription()));
        Mockito.verify(superHeroQuery).searchForId(1l);


    }

    @Test
    @DisplayName("Controlador para Crear superHero")
    public void searchForLikeSuperHero() throws Exception {
        Superhero superhero = new Superhero(1l, "Clark Kent", "Superman", "El Hombre de Acero Regresa");
        Superhero superhero2 = new Superhero(2l, "Peter Parker", "Spiderman", "El Hombre Araña");
        List<Superhero> esperado = Arrays.asList(superhero, superhero2);


        when(superHeroQuery.searchForLikeSuperHero("man")).thenReturn(esperado);

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/superhero/searchlike/{namesuperhero}", "man")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(superhero.getId()))
                .andExpect(jsonPath("$[0].name").value(superhero.getName()))
                .andExpect(jsonPath("$[0].nameSuperhero").value(superhero.getNameSuperhero()))
                .andExpect(jsonPath("$[0].description").value(superhero.getDescription()))
                .andExpect(jsonPath("$[1].id").value(superhero2.getId()))
                .andExpect(jsonPath("$[1].name").value(superhero2.getName()))
                .andExpect(jsonPath("$[1].nameSuperhero").value(superhero2.getNameSuperhero()))
                .andExpect(jsonPath("$[1].description").value(superhero2.getDescription()));

        Mockito.verify(superHeroQuery).searchForLikeSuperHero("man");
    }

    @Test
    @DisplayName("Controlador para Crear superHero")
    public void deleteSuperHero() throws Exception {
        Superhero esperado = new Superhero(1l, "Clark Kent", "Superman", "El Hombre de Acero Regresa");

        mockMvc.perform(MockMvcRequestBuilders.delete("/api/superhero/delete/")
                        .content(asJsonString(esperado))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted());

        Mockito.verify(superHeroQuery).deleteSuperHero(esperado);
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
