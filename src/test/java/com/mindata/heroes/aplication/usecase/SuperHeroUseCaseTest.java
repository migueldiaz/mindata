package com.mindata.heroes.adapter.aplication.usecase;

import com.mindata.heroes.aplication.port.out.*;
import com.mindata.heroes.aplication.usecase.SuperHeroUseCase;
import com.mindata.heroes.domain.Superhero;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class SuperHeroUseCaseTest {

    @Mock
    private SuperHeroCreateRepository superHeroCreateRepository;
    @Mock
    private  SuperHeroUpdateRepository superHeroUpdateRepository;
    @Mock
    private  SuperHeroSearchAllRepository superHeroSearchAllRepository;
    @Mock
    private  SuperHeroSearchForIdRepository superHeroSearchForIdRepository;
    @Mock
    private  SuperHerosearchForLikeRepository superHerosearchForLikeRepository;
    @Mock
    private  SuperHeroDeleteRepository superHeroDeleteRepository;
    @Mock
    private SuperHeroUseCase superHeroUseCase;



    @BeforeEach
    public void setUp(){
        superHeroUseCase = new SuperHeroUseCase(superHeroCreateRepository,
                superHeroUpdateRepository,superHeroSearchAllRepository,superHeroSearchForIdRepository,superHerosearchForLikeRepository,
                superHeroDeleteRepository);
    }


    @Test
    @DisplayName("api para Crear superHero")
    public void createSuperHero() {
        superHeroUseCase.createSuperHero(new Superhero("Clack Ken", "Superman", "El Hombre de Acero"));
        verify(superHeroCreateRepository).createExecute(new Superhero("Clack Ken", "Superman", "El Hombre de Acero"));
    }

    @Test
    @DisplayName("api para Actualizar superHero")
    public void updateSuperHero() {
        superHeroUseCase.updateSuperHero(new Superhero(1l,"Clack Ken", "Superman", "El Hombre de Acero"));
        verify(superHeroUpdateRepository).updateteExecute(new Superhero(1l,"Clack Ken", "Superman", "El Hombre de Acero"));

    }

    @Test
    @DisplayName("api para Listar los superHero")
    public void searchAll() {
        Superhero superhero = new Superhero(1l, "Clark Kent", "Superman", "El Hombre de Acero Regresa");
        Superhero superhero2 = new Superhero(2l, "Peter Parker", "Spiderman", "El Hombre Araña");
        List<Superhero> esperado = Arrays.asList(superhero, superhero2);


        when(superHeroSearchAllRepository.searchAllSuperHero()).thenReturn(esperado);

        final List<Superhero> superheroList = superHeroUseCase.searchAll();
        Assertions.assertEquals(superheroList,esperado);
        Mockito.verify(superHeroSearchAllRepository).searchAllSuperHero();
    }

    @Test
    @DisplayName("api para obtener superHero")
    public void searchForId() {
        Superhero superhero = new Superhero(1l, "Peter Parker", "Spiderman", "El Hombre Araña");
        Superhero esperado = new Superhero(1l, "Peter Parker", "Spiderman", "El Hombre Araña");

        when(superHeroSearchForIdRepository.searchForIdSuperHero(1l)).thenReturn(superhero);
        final Superhero superh = superHeroUseCase.searchForId(1l);
        Assertions.assertEquals(superh,esperado);
        Mockito.verify(superHeroSearchForIdRepository).searchForIdSuperHero(1l);
    }

    @Test
    @DisplayName("api para obtener superHero por Autocompletado")
    public void searchForLikeSuperHero() {
        Superhero superhero = new Superhero(1l, "Clark Kent", "Superman", "El Hombre de Acero Regresa");
        Superhero superhero2 = new Superhero(2l, "Peter Parker", "Spiderman", "El Hombre Araña");
        List<Superhero> esperado = Arrays.asList(superhero, superhero2);
        when(superHerosearchForLikeRepository.searchForLikeSuperHero("man")).thenReturn(esperado);
        final List<Superhero> superheroList = superHeroUseCase.searchForLikeSuperHero("man");
        Assertions.assertEquals(superheroList,esperado);
        Mockito.verify(superHerosearchForLikeRepository).searchForLikeSuperHero("man");
    }

    @Test
    @DisplayName("api para Eliminar superHero")
    public void deleteSuperHero() {
        superHeroUseCase.deleteSuperHero(new Superhero(1l,"Clack Ken", "Superman", "El Hombre de Acero"));
        verify(superHeroDeleteRepository).deleteSuperHero(new Superhero(1l,"Clack Ken", "Superman", "El Hombre de Acero"));

    }


}
