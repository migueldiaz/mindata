# README #

Api Rest Super Heroes minData

Objetivos
-------
* Consultar a todos los súper héroes.
* Consultar un único súper héroe por id.
* Consultar todos los súper héroes que contienen, en su nombre, el valor de un parámetro enviado en la petición. Por ejemplo, si enviamos “man” devolverá “Spiderman”, “Superman”, “Manolito el fuerte”, etc.
* Modificar un súper héroe.
* Eliminar un súper héroe.
* Los súper héroes se deben guardar en una base de datos H2 en memoria.
* La prueba se debe presentar en un repositorio de Git. No hace falta que esté publicado. Se puede pasar comprimido en un único archivo
* Utilizar alguna librería que facilite el mantenimiento de los scripts DDL de base de datos.
* Gestión centralizada de excepciones.
* Poder cachear peticiones.
* Implementar una anotación personalizada que sirva para medir cuánto tarda en ejecutarse.
* Test de integración.
* Test unitarios de algún servicio.
* Presentar la aplicación dockerizada.
* Documentación de la API.


### Tecnologias Implementadas ###

* Java 11
* Spting Boot
* Data Base H2
* Docker
* Maven
* Redis Cache

### Arquitectura  ###

Clean architecture es un conjunto de principios cuya finalidad principal es ocultar los detalles de implementación a la lógica de dominio de la aplicación.

De esta manera mantenemos aislada la lógica, consiguiendo tener una lógica mucho más mantenible y escalable en el tiempo. [leer más](http://xurxodev.com/por-que-utilizo-clean-architecture-en-mis-proyectos/)

## REDIS CACHE DISTRIBUIDO ###
Una caché distribuida es una memoria caché compartida por varios servidores de aplicaciones, que normalmente se mantiene como un servicio externo a los servidores de aplicaciones que tienen acceso a ella.
![img.png](img.png)
[leer mas](https://sacavix.com/2020/11/redis-cache-y-lock-distribuido-ii-cache-distribuida/)

### Servicios  Expuestos ###
~~~
localhost:8087/api/superhero/create

localhost:8087/api/superhero/update

localhost:8087/api/superhero/searchid/{id}

localhost:8087/api/superhero/search

localhost:8087/api/superhero/searchlike/{namesuperhero}

localhost:8087/api/superhero/delete

localhost:8087/api/superhero/caches/{name}
~~~

Parametros de la Aplicación
--------

| Parameter       | Description                                         |
|-----------------|-----------------------------------------------------|
| {id}            | Id Super Heroe Asignación automatica por el sistema |
| {namesuperhero} | Nombre del Super Heroe Ejemplo:(Batman)             |
| {name}          | Nombre de la Key de Cache                           |

Parametros de la Base de Datos H2
--------
| Parameter         | Description                                      |
|-------------------|--------------------------------------------------|
| spring.datasource.url  | url del almacenamiento en memoria                |
| spring.datasource.driverClassName | Driver para la conexión*                         |
| spring.datasource.username    | Usuario (usermin)                                   |
| spring.datasource.password    | Contraseña (passdata)                              |
|spring.sql.init.data-locations   | Ubicacion del archivo sql para generar consultas |

Puede Acceder aplicacion de la BD a través 
~~~
http://localhost:8087/h2-console
~~~
Flujo de detección de información
------------
### Crear Super Heroes

| Parameter | Description                         |
| ------------- |-------------------------------------|
| Método  | POST                                |
| URL  | localhost:8087/api/superhero/create |

1. ingresa a la url `localhost:8087/api/superhero/create`
2. Colocamos en el Body el json con la información del Super Heroe
```json
{   
  "name": "string",
  "nameSuperhero": "string",
  "description": "string" 
}
```
3. Guarda el Super Heroe

### Actualiza El super Heroe


| Parameter | Description           |
| ------------- |-----------------------|
| Método  | PUT                   |
| URL  | localhost:8087/api/superhero/update |


1. ingresa a la url `localhost:8087/api/superhero/update`
2. Colocamos en el Body el json con la información del Super Heroe
```json
{   
  "id": "long",
  "name": "string",
  "nameSuperhero": "string",
  "description": "string" 
}
```
3. Modifica el Super Heroe
4. Actualiza el Cache

### Busqueda de Todos los Super Heroe 


| Parameter | Description                  |
| ------------- |------------------------------|
| Método  | GET                          |
| URL  | localhost:8087/api/superhero/search   |


1. ingresa a la url `localhost:8087/api/superhero/update`
2. Obtenemos los Super Heroes
```json
[
{
  "id": 1,
  "createdDate": "2023-01-25T17:23:38.402Z",
  "name": "string",
  "nameSuperhero": "string",
  "description": "string",
  "lastUpdate": "2023-01-25T17:23:38.402Z"
},
  {
    "id": 2,
    "createdDate": "2023-01-25T17:23:38.402Z",
    "name": "string",
    "nameSuperhero": "string",
    "description": "string",
    "lastUpdate": "2023-01-25T17:23:38.402Z"
  }
  
]
```
4. Actualiza el Cache
5. Consulta mas Rápida

### Busqueda por ID de Super Heroe

| Parameter | Description                 |
| ------------- |-----------------------------|
| Método  | GET                         |
| URL  | localhost:8087/api/superhero/searchid/{id}  |


1. ingresa a la url `localhost:8087/api/superhero/searchid/{id}`
2. Remplazamos el parametro por el numero de id del Super Heroe
3. Obtenemos  Super Heroes
```json
{
  "id": 1,
  "createdDate": "2023-01-25T17:23:38.402Z",
  "name": "string",
  "nameSuperhero": "string",
  "description": "string",
  "lastUpdate": "2023-01-25T17:23:38.402Z"
}
```
4. Actualiza el Cache
5. Consulta mas Rápida

### Busqueda por nameSuperHero de Super Heroe

| Parameter | Description                 |
| ------------- |-----------------------------|
| Método  | GET                         |
| URL  | localhost:8087/api/superhero/searchlike/{namesuperhero}  |


1. ingresa a la url `localhost:8087/api/superhero/searchlike/{namesuperhero}`
2. Remplazamos el parametro namesuperhero por la palabra clave ejemplo "man" del Super Heroe
3. Obtenemos  todos los Super Heroes que tenga la palabra clave.
```json
[
  {
    "id": 1,
    "createdDate": "2023-01-25T17:23:38.402Z",
    "name": "string",
    "nameSuperhero": "string",
    "description": "string",
    "lastUpdate": "2023-01-25T17:23:38.402Z"
  },
  {
    "id": 2,
    "createdDate": "2023-01-25T17:23:38.402Z",
    "name": "string",
    "nameSuperhero": "string",
    "description": "string",
    "lastUpdate": "2023-01-25T17:23:38.402Z"
  }

]
```
4. Actualiza el Cache
5. Consulta mas Rápida
### Borrar Super Heroe

| Parameter | Description           |
| ------------- |-----------------------|
| Método  | DELETE                |
| URL  | localhost:8087/api/superhero/delete |


1. ingresa a la url `/api/superhero/delete`
2. Colocamos en el Body el json con la información del Super Heroe
3. Borramos el Super Heroes
```json
{
  "id": 1,
  "name": "string",
  "nameSuperhero": "string",
  "description": "string"
}
```
4. Actualiza el Cache

### Borrar Cache de Redis

| Parameter | Description                                |
| ------------- |--------------------------------------------|
| Método  | PATCH                                      |
| URL  | localhost:8087/api/superhero/caches/{name} |


1. ingresa a la url `localhost:8087/api/superhero/caches/{name}`
2. Remplazamos el parametro por el nombre de la key.
3. Obtenemos  Super Heroes
4. Limpia todo el cache

Anotación @TimeSuper
--------
  Esta anotación personalizada se creo con la finalidad de medir el tiempo de ejecucion de un proceso.
  Se ejecuta a nivel de métodos no a nivel de clases, en su logica verifica que el metodo de la clase tenga la anotacion @timeSumer, imprime en un log el tiempo de la consulta.

Compilación
--
### [docker-compose.yml](docker-compose.yml)
 Esta definida el contenedor que ejecuta la aplicación, la cual la imagen se crea y se expone.

### [Dockerfile](Dockerfile)
Contiene las instrucciones necesarias para crear una nueva imagen del contenedor

### Swagger ###
 Con la URL expuesta a continuación puede acceder a Swagger y poder hacer pruebas en tiempo de ejecución de cada uno de los endpoint con la herramienta 

URL
http://localhost:8087/api/superhero/swagger-ui-custom.html

Requerimientos
--
1. Intalar Java 11
2. Instalar Maven 3.6
3. Instalar Docker
4. Redis Cache

